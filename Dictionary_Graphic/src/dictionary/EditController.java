package dictionary;

import com.jfoenix.controls.JFXButton;
import dictionary.database.DatabaseHandler;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class EditController implements Initializable {

    @FXML
    private HTMLEditor htmlEditor;

    @FXML
    private Text text;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton cancel;

    private DatabaseHandler databaseHandler;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setDatabaseHandler(DatabaseHandler db) {
        this.databaseHandler = db;
    }

    public void setWord(String word) {
        text.setText(word);
    }

    public void setHtmlEditor(String text) {
        htmlEditor.setHtmlText(text);
    }

    public void saveWord(Event e) {
        try {
            if (htmlEditor.getHtmlText().isEmpty()) System.out.println("Detail cannot be empty");
            else {
                databaseHandler.saveWord(text.getText(), htmlEditor.getHtmlText());
                Stage stage = (Stage) save.getScene().getWindow();
                stage.close();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void cancel(Event e) {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }
}
