package dictionary;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import dictionary.database.DatabaseHandler;
import dictionary.google_api.Api;
import dictionary.text_to_speech.TextToSpeech;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.print.DocFlavor;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Controller {
    WebEngine engine;
    TextToSpeech tts;
    private DatabaseHandler db;
    @FXML
    private JFXTextField search;
    @FXML
    private WebView webView;
    @FXML
    private ListView<String> listView;
    @FXML
    private JFXButton online;
    @FXML
    private ImageView audio;
    @FXML
    private ImageView searchImage;

    @FXML
    void initialize() {
        db = new DatabaseHandler();
        engine = webView.getEngine();
        webView.setDisable(true);
        tts = new TextToSpeech();
        //tts.setVoice("cmu-slt-hsmm");
        tts.setVoice("dfki-poppy-hsmm");
    }

    @FXML
    public void searchWord() {
        try {
            String word = search.getText();
            if (!word.isEmpty()) {
                ResultSet res = db.getWord(word);
                if (res.next()) {
                    engine.loadContent(res.getString("detail"));
                } else {
                    URL url = this.getClass().getResource("html/index.html");
                    engine.load(url.toString());
                }
            }
        } catch (SQLException error) {
            db.AlertError(error.getMessage());
        }
    }

    @FXML
    public void enterPressed(KeyEvent e) {
        try {
            if (e.getCode() == KeyCode.ENTER) {
                String word = search.getText();
                if (!word.isEmpty()) {
                    ResultSet res = db.getWord(word);
                    if (res.next()) {
                        engine.loadContent(res.getString("detail"));
                    } else {
                        URL url = this.getClass().getResource("html/index.html");
                        engine.load(url.toString());
                    }
                }
            }
        } catch (SQLException error) {
            db.AlertError(error.getMessage());
        }
    }

    @FXML
    public void keyPressed(KeyEvent e) {
        try {
            String word = search.getText();
            if (!word.isEmpty()) {
                listView.getItems().clear();
                ResultSet res = db.getWords(word);
                while (res.next()) {
                    listView.getItems().add(res.getString("word"));
                }
            }
        } catch (SQLException error) {
            db.AlertError(error.getMessage());
        }
    }

    @FXML
    public void searchClickedItem(MouseEvent e) {
        try {
            String word = listView.getSelectionModel().getSelectedItem();
            ResultSet res = db.getWord(word);
            if (res.next()) {
                engine.loadContent(res.getString("detail"));
                search.setText(word);
            } else {
                URL url = this.getClass().getResource("html/index.html");
                engine.load(url.toString());
            }

        } catch (Exception ex) {
            AlertError(ex.getMessage());
        }
    }

    @FXML
    public void editWord(Event e) {
        try {
            String word = search.getText();
            if (word.isEmpty()) return;
            else {
                ResultSet res = db.getWord(word);
                if (res.next()) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("edit.fxml"));
                    Parent root1 = loader.load();
                    Stage stage = new Stage();
                    stage.setTitle("Edit");
                    stage.setScene(new Scene(root1));
                    stage.setMaxWidth(573);
                    stage.setMaxHeight(811);
                    stage.setWidth(573);
                    stage.setHeight(811);
                    stage.show();
                    EditController editController = loader.getController();
                    editController.setWord(word);
                    editController.setHtmlEditor(res.getString("detail"));
                    editController.setDatabaseHandler(db);
                } else {
                    AlertError(word + " is a new word.\nPlease click Add to add a new word", "Error");
                }
            }
        } catch (Exception ex) {
            AlertError(ex.getMessage());
        }
    }

    @FXML
    public void addWord(Event e) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("add.fxml"));
            Parent root2 = loader.load();
            Stage stage = new Stage();
            stage.setTitle("Add");
            stage.setScene(new Scene(root2));
            stage.setMaxWidth(573);
            stage.setMaxHeight(811);
            stage.setWidth(573);
            stage.setHeight(811);
            stage.show();
            AddController addController = loader.getController();
            addController.setDatabaseHandler(db);
        } catch (Exception ex) {
            AlertError(ex.getMessage());
        }
    }

    @FXML
    public void switchOnline(Event event) throws IOException {
        if (TestConnection()) {
            Parent root3 = FXMLLoader.load(getClass().getResource("google.fxml"));
            Scene onlineSearch = new Scene(root3);
            Stage stage;
            if (event.getSource() == online) {
                stage = (Stage) online.getScene().getWindow();
                stage.setScene(onlineSearch);
                stage.show();
            }
        } else {
            AlertError("Unable to reach the Internet", "Network Error");
        }
    }

    @FXML
    public void speakWord() {
        if (search.getText().isEmpty())
            tts.speak("There is nothing to speak. Please fill in the search bar", 2.0f, false, false);
        else tts.speak(search.getText(), 2.0f, false, false);
    }

    public boolean TestConnection() {
        Socket socket = new Socket();
        InetSocketAddress addr = new InetSocketAddress("www.google.com", 80);
        try {
            socket.connect(addr, 3000);
            socket.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void AlertError(String err, String title) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(err);
        alert.showAndWait();
    }

    public void AlertError(String err) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(err);
        alert.showAndWait();
    }

    public void AlertSuccess(String msg) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }

}
