package dictionary.database;

import javafx.scene.control.Alert;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//(id TEXT primary key, title TEXT, author TEXT, publisher TEXT, intcode TEXT, isAvail INTEGER)
public class DatabaseHandler {
    private static String library_db = "jdbc:sqlite:dictionaryE_V.db";

    private static Connection conn;
    private static Statement statement;

    public DatabaseHandler() {
        try {
            conn = DriverManager.getConnection(library_db);
            statement = conn.createStatement();
            System.out.println("Successfully established connection to database");
        } catch (SQLException e) {
            System.out.println("SQL Exception " + e.getMessage());
        }
    }

    public ResultSet getWord(String query) {
        System.out.println(query);
        if (!query.isEmpty()) {
            try {
                return statement.executeQuery("SELECT * FROM dictionary WHERE word = " + "'" + query + "'");
            } catch (SQLException e) {
                AlertError(e.getMessage());
                return null;
            }
        }
        return null;
    }

    public ResultSet getWords(String query) {
        if (!query.isEmpty()) {
            try {
                return statement.executeQuery("SELECT * FROM dictionary WHERE word LIKE " + "'" + query + "%'" + " LIMIT 25 OFFSET 0");
            } catch (SQLException e) {
                AlertError(e.getMessage());
                return null;
            }
        }
        return null;
    }

    public void saveWord(String word, String detail) {
        try {
            System.out.println(detail);
            String st = "UPDATE dictionary SET detail=" + "'" + detail + "' WHERE word = " + "'" + word + "'";
            int rows = statement.executeUpdate(st);
            if(rows > 0) AlertSuccess("Sửa từ thành công!");
        } catch (SQLException ex) {
            AlertError(ex.getMessage());
        }
    }

    public void insertWord(String word, String detail) {
        try {
            String st = "INSERT INTO dictionary(word, detail) VALUES (" + "'" + word + "'," + "'" + detail + "')";
            int row = statement.executeUpdate(st);
            if(row > 0) AlertSuccess("Thêm từ thành công!");
        } catch (SQLException ex) {
            AlertError(ex.getMessage());
        }
    }


    public void AlertError(String err) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText("SQLException Error: " + err);
        alert.showAndWait();
    }

    public void AlertSuccess(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Success");
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
