package dictionary;

import com.jfoenix.controls.JFXButton;
import dictionary.database.DatabaseHandler;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;

import java.awt.*;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

public class AddController implements Initializable {

    @FXML
    private HTMLEditor htmlEditor;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton cancel;

    @FXML
    private TextField textField;
    private DatabaseHandler databaseHandler;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setDatabaseHandler(DatabaseHandler db) {
        this.databaseHandler = db;
    }

    public void insertWord(Event e) {
        try {
            if (htmlEditor.getHtmlText().isEmpty()) System.out.println("Detail cannot be empty");
            else {
                String word = textField.getText();
                if (word.isEmpty()) System.out.println("Word cannot be empty");
                else {
                    ResultSet res = databaseHandler.getWord(word);
                    if(res.next()) {
                        System.out.println(res.getString("word") + " already in database");
                    } else {
                        databaseHandler.insertWord(textField.getText(), htmlEditor.getHtmlText());
                        Stage stage = (Stage) save.getScene().getWindow();
                        stage.close();
                    }
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void cancel(Event e) {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }
}
