package dictionary;

import com.jfoenix.controls.JFXButton;
import dictionary.google_api.Api;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class GoogleController {

    @FXML
    private JFXButton btn_offline;

    @FXML
    private JFXButton translate;

    @FXML
    private TextArea left;

    @FXML
    private TextArea right;


    private Api google;

    public void initialize() {
        google = new Api();
        left.setWrapText(true);
        right.setWrapText(true);
    }

    @FXML
    void offlineSearch(MouseEvent event) throws IOException {
        Parent root3 = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene onlineSearch = new Scene(root3);
        Stage stage;
        if (event.getSource() == btn_offline) {
            stage = (Stage) btn_offline.getScene().getWindow();
            stage.setScene(onlineSearch);
            stage.show();
        }
    }

    @FXML
    void translate(MouseEvent event) throws Exception{
        String text = left.getText();
        String result = google.translate(text);
        right.setText(result);
    }

}
