# Từ điển Anh-Việt

### Link file jar phiên bản đồ họa
```
https://drive.google.com/file/d/1A8pnzeifiPbPg7ZHbMmB0x5BHJQwbleh/view?usp=sharing
```

## Built With

java version "1.8.0_181"
Java(TM) SE Runtime Environment (build 1.8.0_181-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)

## Authors

* **Trần Đức Hiếu** 
* **Bùi Tiến Đạt** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

