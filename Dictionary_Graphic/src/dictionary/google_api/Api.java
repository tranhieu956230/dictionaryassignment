package dictionary.google_api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

public class Api {

    private static HttpURLConnection con;
    private String result = "";

    public String translate(String word) throws Exception {

        String url = "https://translation.googleapis.com/language/translate/v2";
        String urlParameters = "q=" + word + "&target=vi&key=AIzaSyCO-JjgjnBnh_Z9lyFHD-9aWok-J67Qh5E";
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

        try {

            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();

            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject myResponse = new JSONObject(response.toString());

            JSONObject data = myResponse.getJSONObject("data");
            result = data.getJSONArray("translations").getJSONObject(0).getString("translatedText");


        } finally {
            con.disconnect();
            return result;
        }
    }
}